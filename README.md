Złap balonik!
=============
Gra polegająca na łapaniu balonika na autostradzie za pomocą kosiarki, unikając rozjechania przez samochody.

### Sterowanie
Strzałki, Enter, Escape.

Instalacja bibliotek
--------------------
Projekt do działania wymaga:
* SDL2  https://www.libsdl.org/index.php
* SDL2 Image Library https://www.libsdl.org/projects/SDL_image/
* SDL2 TTF https://www.libsdl.org/projects/SDL_ttf/

### Linux
```
$ apt install libsdl2-2.0-0 libsdl2-dev libsdl2-image-2.0-0 libsdl2-image-dev libsdl2-ttf-2.0-0 libsdl2-ttf-dev
```

### MacOS
```
$ brew install sdl2
$ brew install sdl2_image
$ brew install sdl2_tft
```

Ustawienia CMake do budowania
-----------------------------
Niestety z nieokreślonego powodu CMake nie chciał w moim środowisku znaleźć zainstalowanych bibliotek. Nawet jak miał podaną ścieżkę absolutną w PKG_SEARCH_MODULE. Poddałem się drugiego wieczoru i ustaliłem w CMakeLists.txt ręcznie ścieżki do bibliotek.

To samo będzie najprawdopodobniej potrzebne przy próbie uruchomienia tego projektu w innym środowisku. Przykłady są w pliku.

Traktuję to jako osobistą porażkę. Nie jakąś dużą. Taką pomiędzy "zaspałem" i "zgubiłem długopis".

Co się udało
------------
Podstawowe założenia zostały spełnione. Gracz steruje małym pojazdem. Łapie balonik za punkty. "Ginie" przy kontakcie z samochodami.

Balonik może porusza się bardzo nerwowo, ale jego kontakt z samochodami daje wrażenie poddawaniu się podmuchom wiatru. Przynajmniej jeśli mamy dobry dzień i chcemy to dostrzec.

Co się nie udało
----------------
Brak trybu multiplayer. Biorąc pod uwagę zwrotność pojazdu gracza niczym wózek z Lidla, tryb multi mógłby zdecydowanie zmienić odbiór gry. Myślę, że na lepsze.

Użyte resources
---------------
Przy wykorzystaniu niekomercyjnym użyte materiały są zgodne z ich licencją.
* Sprite'y samochodów https://opengameart.org/content/top-view-car-truck-sprites
* Czcionka Orena http://www.fontspace.com/qbotype-fonts/orena
* Czcionka Lucon https://www.fontsupply.com/fonts/L/Lucon.html

Reszta obrazów jest mojego autorstwa. 

Być może powinienem był to przemilczeć.

