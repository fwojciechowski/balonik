#include <random>
#include "Baloon.h"
#include "Player.h"


Baloon::Baloon() = default;


Baloon::Baloon(int x, int y) {
    std::random_device rd;
    std::mt19937 rng(rd());
    _seed = rng;

    _x = x;
    _y = y;
}

void Baloon::move(double delta) {
    std::uniform_int_distribution<int> rndChance(0, 99);

    // Check level boundaries for baloon
    if (_x < 0) {_x = 0; _rotation += 90;}
    else if (_x > SCREEN_WIDTH) {_x = SCREEN_WIDTH; _rotation += 90; _velocity += 50;}

    if (_y < 0) {_y = 0; _rotation += 90;}
    else if (_y > SCREEN_HEIGHT) {_y = SCREEN_HEIGHT; _rotation += 90; _velocity += 50;}


    if (rndChance(_seed) < 5) {
        _rotation -= 10 * PLAYER_MAX_ROTATION_SPEED * _velocity/PLAYER_MAX_VELOCITY * delta;
    }
    else if (rndChance(_seed) < 10) {
        _rotation += 10 * PLAYER_MAX_ROTATION_SPEED * _velocity/PLAYER_MAX_VELOCITY * delta;
    }


    if (_velocity > 150) _velocity -= _velocity * delta;


    Car::move(delta);
}

void Baloon::addVelocity(int inV) {
    _velocity += inV;
}


