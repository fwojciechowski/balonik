#ifndef BALONIK_PLAYER_H
#define BALONIK_PLAYER_H


#include "Car.h"

extern const int PLAYER_MAX_VELOCITY;
extern const int PLAYER_MAX_ACCELERATION;
extern const int PLAYER_MAX_ROTATION_SPEED;

/**
 * Represents Player physical instance.
 */
class Player : public Car {
public:
    Player();
    /**
     * Create Player instance at position.
     * @param x
     * @param y
     */
    Player(int x, int y);
    /**
     * Reset Player object. Use at start of the game and beginning of new round. Resets movement of the Player.
     */
    void reset();
    /**
     * Process keyboard events. Checks for pressed keys and moves Player instance appropriately.
     * @param e Event description from SDL Library
     */
    void processEvents(SDL_Event e);
    /**
     * Overrides Car movements - first modify Player movement (acceleration, braking, velocity etc.) and then proceed
     * to move it using Car class method.
     * @param delta Time difference between last and current frame.
     */
    void move(double delta);

private:
    bool _accelerate = false;
    bool _brake = false;
    bool _turnLeft = false;
    bool _turnRight = false;

    Uint32 _reverseTimer = 0; // to set when cart can start reversing
};


#endif //BALONIK_PLAYER_H
