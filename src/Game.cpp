#include "Game.h"
#include <chrono>
#include <thread>
#include <string>
#include <SDL_image.h>
#include <SDL_ttf.h>

extern SDL_Window* gWindow;
extern SDL_Renderer* gRenderer;
extern TTF_Font* gFont;


Game::Game(const int screenWidth, const int screenHeight, const int frameRate) {
    _screenWidth = screenWidth;
    _screenHeight = screenHeight;
    _frameRate = frameRate;

    _lastFrameTime = SDL_GetTicks();
}


Game::Game() = default;


bool Game::Initialize() {
    //Initialization flag
    bool success = true;

    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
        success = false;
    }
    else
    {
        //Create window
        gWindow = SDL_CreateWindow( "Zlap Balonik!", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, Game::_screenWidth, Game::_screenHeight, SDL_WINDOW_SHOWN );
        if( gWindow == nullptr )
        {
            printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
            success = false;
        }
        else
        {
            //Create renderer for window
            gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );

            if( gRenderer == nullptr )
            {
                printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
                success = false;
            }
            else
            {
                // Check if VSYNC is available. If not - turn on frame limiter.
                SDL_RendererInfo rendererInfo;

                if ( SDL_GetRendererInfo(gRenderer, &rendererInfo) != 0 ) {
                    printf( "Couldn't get renderer info! SDL Error: %s\n", SDL_GetError() );
                }
                else
                {
                    if ( rendererInfo.flags != SDL_RENDERER_PRESENTVSYNC )
                    {
                        printf("VSYNC is not available.\n");
                    }
                    else
                    {
                        printf("VSYNC is available.\n");
                    }
                }

                //Initialize renderer color
                SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

                //Initialize PNG loading
                int imgFlags = IMG_INIT_PNG;
                if( !( IMG_Init( imgFlags ) & imgFlags ) )
                {
                    printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
                    success = false;
                }

                //Initialize SDL_ttf
                if( TTF_Init() == -1 )
                {
                    printf( "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError() );
                    success = false;
                }
            }
        }
    }

    return success;
}


void Game::Run() {
#ifdef DEBUG_MODE
    Uint32 framesCounter = 0;
#endif

    //Start up SDL and create window
    if( !Initialize() )
    {
        printf( "Failed to initialize!\n" );
    }
    else
    {
        // Initialize random number generator
        std::random_device rd;
        std::mt19937 rng(rd());
        _seed = rng;

        // Initialize font
        gFont = TTF_OpenFont( "resources/lucon.ttf", 22 );
        if( gFont == nullptr )
        {
            printf( "Couldn\'t open font! SDL_ttf Error: %s\n", TTF_GetError() );
        }


        // Initialize menu
        std::list<menuItem> menuOptions;
        menuOptions.push_back(Menu::createMenuItem("Start Game", START_GAME));
        menuOptions.push_back(Menu::createMenuItem("Resume Game", RESUME_GAME));
        menuOptions.push_back(Menu::createMenuItem("Stop Game", STOP_GAME));
        menuOptions.push_back(Menu::createMenuItem("Exit Game", EXIT_GAME));

        _menu = Menu(menuOptions);

        _menu.open(_gameInProgress);

        // Initialize background
        GameObject background = GameObject(0,0);
        background.createFromImage("resources/background.png");
        _gameObjectList.push_back(&background);


        // Initialize Title
        GameObject title = GameObject();
        title.createFromImage("resources/splash-screen.png");
        title.setPosition(_screenWidth/2, _screenHeight/2);
        _gameObjectList.push_back(&title);


        // Initialize Player
        Player p = Player(_screenWidth / 8, _screenHeight / 2);
        p.createFromImage("resources/player1.png");
        _p = p;


        // Initialize Baloon
        Baloon b = Baloon(_screenWidth / 2, _screenHeight / 2);
        b.createFromImage("resources/baloon.png");
        _b = b;

        // Initialize rounds
        GameObject goRound = GameObject(0, 0);
        goRound.createFromText("Round: ");
        _goRound = goRound;

        // Initialize points
        GameObject goPoints = GameObject(0, 25);
        goPoints.createFromText("Points: ");
        _goPoints = goPoints;

        // Initialize final score
        GameObject goFinalScore = GameObject();
        goFinalScore.createFromText("Final Score: ", TTF_OpenFont( "resources/lucon.ttf", 50 ));
        _goFinalScore = goFinalScore;


#ifdef DEBUG_MODE
        // Initialize FPS Counter
        GameObject goFPSText = GameObject(0, 50);
        goFPSText.createFromText("FPS: ");
        _gameObjectList.push_back(&goFPSText);

        std::stringstream fpsText;
        fpsText.str( "" );

        // Initialize Player position info
        GameObject goPlayerPositionText = GameObject(0, 75);
        goPlayerPositionText.createFromText("Px Py");
        _gameObjectList.push_back(&goPlayerPositionText);

        std::stringstream playerPositionText;
        playerPositionText.str( "" );

        // Initialize Collision indicator
        GameObject goCollisionText = GameObject(0, 100);
        goCollisionText.createFromText("Collision: ");
        _gameObjectList.push_back(&goCollisionText);

        std::stringstream collisionText;
        collisionText.str( "" );
#endif
        // Initialize starting cars
        for (int i=0; i<8; i++) {
            std::uniform_int_distribution<int> rndXPosition(0, _screenWidth);
            SDL_Point p = {rndXPosition(_seed), CAR_SPAWN_POINTS[i].y};
            spawnCar(p);
        }

        // Main loop of the game.
        while( _running )
        {
#ifdef DEBUG_MODE
            fpsText << framesCounter / ( SDL_GetTicks() / 1000.f ) << " FPS";
            goFPSText.setText(fpsText.str());

            playerPositionText << "PX: " << (int) _p.getPositionX() << " PY: " << (int) _p.getPositionY();
            goPlayerPositionText.setText(playerPositionText.str());

            collisionText << "Collision: " << (_playerCarCollision ? "True" : "False");
            goCollisionText.setText(collisionText.str());
#endif
            Frame();
#ifdef DEBUG_MODE
            fpsText.str("");
            fpsText.clear();

            playerPositionText.str("");
            playerPositionText.clear();

            collisionText.str("");
            collisionText.clear();

            framesCounter++;
#endif
        }
    }

    //Free resources and close SDL
    Shutdown();
}


void Game::Frame() {
    std::chrono::steady_clock::time_point startFrameTime;

    if (FRAME_LIMITER) {
        // Write starting time for a function for frame limit control
        startFrameTime = std::chrono::steady_clock::now();
    }

    // Used to calculate movement etc. Must be frame rate independent.
    double deltaTime = (double)(SDL_GetTicks() - _lastFrameTime) / 1000;
    _lastFrameTime = SDL_GetTicks();

    //Handle events on queue
    ProcessEvents(_event);

    // Move objects if game is running
    if (!_paused) {
        for (Car& car : _carList) {
            car.move(deltaTime);
        }

        _b.move(deltaTime);

        _p.move(deltaTime);

        if (_gameInProgress) checkCollisions(deltaTime);
    }

    // Handle Points and Round if game in progress
    if (_gameInProgress) {
        _roundText.str("");
        _roundText.clear();

        _pointsText.str("");
        _pointsText.clear();

        _roundText << "Round: " << _round;
        _goRound.setText(_roundText.str());

        _pointsText << "Score: " << _points;
        _goPoints.setText(_pointsText.str());
    }

    Render();

    // Grim Reaper - remove cars which fulfilled their purpose.
    {
        auto i = _carList.begin();
        while (i != _carList.end())
        {
            if (!(*i).isAlive()) {
                _carList.erase(i++);
            }
            else
            {
                ++i;
            }
        }
    }

    // Set 10% chance to spawn a car
    std::uniform_int_distribution<int> rndChance(0, 99);
    if (_carList.size() < CAR_MAX_COUNT && rndChance(_seed) < 10) {
        spawnCar();
    }

    if (FRAME_LIMITER) {
        // Frame limit control. Sleep if frame took shorter to render than it "should".
        std::chrono::steady_clock::time_point endFrameTime = std::chrono::steady_clock::now();

        if (endFrameTime - startFrameTime < std::chrono::milliseconds(1000/_frameRate))
            std::this_thread::sleep_for(std::chrono::milliseconds(1000/_frameRate) - (endFrameTime - startFrameTime));
    }
}


void Game::ProcessEvents(SDL_Event e) {
    while( SDL_PollEvent( &e ) != 0 )
    {
        //User requests quit
        if( e.type == SDL_QUIT )
        {
            _running = false;
        }

        if (_roundInProgress) _p.processEvents(e);

        if (_menu.isOpen()) {
            int result = _menu.processEvents(e);

            if (result != NONE) {
                switch (result) {
                    case START_GAME:
                        newGame();
                        break;

                    case RESUME_GAME:
                        if (_gameInProgress) {
                            _roundInProgress = true;
                            _paused = false;
                        }
                        break;

                    case STOP_GAME:
                        stopGame();
                        break;

                    case EXIT_GAME:
                        _running = false;
                        break;

                    default:
                        break;
                }
            }
        }
        else if( e.type == SDL_KEYUP && e.key.keysym.sym == SDLK_ESCAPE) {
            _roundInProgress = false;
            if (_gameInProgress) _paused = true;
            _menu.open(_gameInProgress);
        };
    }
}


void Game::checkCollisions(double deltaTime) {
    if ( SDL_HasIntersection(_p.getBoundingBox(), _b.getBoundingBox()) == SDL_TRUE )
    {
        _playerCarCollision = true;
        endRound(true);
        return;
    }
    else
    {
        _playerCarCollision = false;
    }

    for (Car& car : _carList) {
        if ( SDL_HasIntersection(_p.getBoundingBox(), car.getBoundingBox()) == SDL_TRUE )
        {
            _playerCarCollision = true;
            endRound(false);
            break;
        }
        else
        {
            _playerCarCollision = false;
        }

        if ( SDL_HasIntersection(_b.getBoundingBox(), car.getBoundingBox()) == SDL_TRUE )
        {
            _b.addVelocity(int(1000*deltaTime));
        }
    }
}


void Game::spawnCar() {
    // Find spawning point
    std::uniform_int_distribution<int> rndPoint(0, 7);
    int spawningPoint = rndPoint(_seed);

    if (SDL_GetTicks() - _lastCarSpawningTime[spawningPoint] > 1500) {
        _lastCarSpawningTime[spawningPoint] = SDL_GetTicks();
        spawnCar(CAR_SPAWN_POINTS[spawningPoint]);
    }
}


void Game::spawnCar(SDL_Point p) {
    std::uniform_int_distribution<int> rndTexture(0, 4);
    std::uniform_int_distribution<int> rndVelocity(int(0.75 * CAR_MAX_VELOCITY), CAR_MAX_VELOCITY);

    Car car = Car(p.x, p.y);
    car.createFromImage(CAR_TEXTURES[rndTexture(_seed)]);

    if (p.x > _screenWidth) {
        car.setRotation(90);
    } else {
        car.setRotation(-90);
    }

    car.setVelocity(rndVelocity(_seed));
    car.initBoundingBox();
    _carList.push_back(car);
}


void Game::Render() {
    //Clear screen
    SDL_RenderClear( gRenderer );

    for (GameObject* go : _gameObjectList) {
        go->Render();
    }

    for (Car& car : _carList) {
        car.Render();
    }

    if (_gameInProgress) {
        _p.Render();

        _b.Render();

        _goPoints.Render();

        _goRound.Render();
    }

    if (_gameEnded && !(_menu.isOpen())) _goFinalScore.Render();

    if (_menu.isOpen()) _menu.render();

    //Update screen
    SDL_RenderPresent( gRenderer );
}


void Game::Shutdown()
{
    // Destroy renderer
    SDL_DestroyRenderer( gRenderer );
    gRenderer = nullptr;

    //Destroy window
    SDL_DestroyWindow( gWindow );
    gWindow = nullptr;

    //Quit SDL subsystems
    IMG_Quit();
    SDL_Quit();
}

void Game::newGame() {
    _gameEnded = false;

    // Set points and round
    _points = 0;
    _round = 1;

    resetGame();

    _gameInProgress = true;

    _roundInProgress = true;
}

void Game::stopGame() {
    _paused = false;
    _gameInProgress = false;
    _roundInProgress = false;

    showEndScore();
}

void Game::endRound(bool success) {
    if (success) {
        _points++;
    }

    if (++_round <= MAX_ROUND) resetGame();
    else stopGame();
}

void Game::resetGame() {
    // Prepare Player
    _p.setPosition(_screenWidth / 8, _screenHeight / 2);
    _p.setRotation(-90);
    _p.initBoundingBox();
    _p.reset();


    // Prepare Baloon
    _b.setPosition(_screenWidth / 2, _screenHeight / 2);
    _b.setRotation(0);
    _b.initBoundingBox();
    _b.setVelocity(200);
}

void Game::showEndScore() {
    _finalScoreText.str("");
    _finalScoreText.clear();

    _finalScoreText << "Final Score: " << _points;
    _goFinalScore.setText(_finalScoreText.str());
    _goFinalScore.setPosition(_screenWidth/2, _screenHeight/2);

    _gameEnded = true;
}
