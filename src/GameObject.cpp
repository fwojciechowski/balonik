#include "GameObject.h"

#include <utility>

extern SDL_Window* gWindow;


GameObject::GameObject() = default;

GameObject::GameObject(int x, int y) {
    _x = x;
    _y = y;
}

void GameObject::createFromImage(std::string path, double angle) {
    _s = Sprite(std::move(path), (int) _x, (int) _y, angle, SPRITE_TYPE_IMAGE);
}

void GameObject::createFromText(std::string text, TTF_Font* font) {
    _text = std::move(text);
    _s = Sprite(_text, (int) _x, (int) _y, 0, SPRITE_TYPE_TEXT, font);
}

void GameObject::setPosition(double x, double y) {
    if (x < -100 || y < -100 || x > SCREEN_WIDTH+100 || y > SCREEN_HEIGHT+100) markForDeath();

    _x = x;
    _y = y;

    _s.setPosition((int)x, (int)y);
}

void GameObject::setText(std::string text) {
    _text = std::move(text);
    _s.modifyTexture(_text, SPRITE_TYPE_TEXT);
}