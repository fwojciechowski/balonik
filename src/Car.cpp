#include "Car.h"

extern SDL_Renderer* gRenderer;

void Car::move(double delta) {

    setPosition(
            getPositionX() - ( sin( _rotation * ( M_PI / 180 ) ) * _velocity * delta ),
            getPositionY() + ( cos( _rotation * ( M_PI / 180 ) ) * _velocity * delta )
    );

    setAngle(_rotation);

    refreshBoundingBox();
}

Car::Car(int x, int y) {
    _x = x;
    _y = y;
}


void Car::refreshBoundingBox() {
    _bBox.x = (int) (_x - _bBox.w/2.f);
    _bBox.y = (int) (_y - _bBox.h/2.f);
}

void Car::Render() {
#ifdef DEBUG_MODE
        // render bounding boxes
        SDL_SetRenderDrawColor(gRenderer, 0, 255, 0, SDL_ALPHA_OPAQUE);
        SDL_RenderDrawRect(gRenderer, &_bBox);
        SDL_SetRenderDrawColor(gRenderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
#endif

    GameObject::Render();
}

void Car::setVelocity(double velocity) {
    _velocity = velocity;
}

void Car::setRotation(double rotation) {
    _rotation = rotation;
}

void Car::initBoundingBox() {
    Sprite* sp = getSprite();

    _bBox.x = (int) _x;
    _bBox.y = (int) _y;

    if (sp->getSpriteWidth() > sp->getSpriteHeight())
    {
        _bBox.w = sp->getSpriteWidth();
        _bBox.h = sp->getSpriteHeight();
    }
    else
    {
        _bBox.w = sp->getSpriteHeight();
        _bBox.h = sp->getSpriteWidth();
    }
}

Car::Car() = default;
