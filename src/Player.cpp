#include "Player.h"
#include <cmath>


Player::Player() = default;


Player::Player(int x, int y) {
    _x = x;
    _y = y;
}


void Player::processEvents(SDL_Event e) {
    if( e.type == SDL_KEYDOWN ) {
        switch (e.key.keysym.sym) {
            case SDLK_UP:
                _accelerate = true;
                break;

            case SDLK_DOWN:
                if (!_brake) _reverseTimer = SDL_GetTicks();
                _brake = true;
                break;

            case SDLK_LEFT:
                _turnLeft = true;
                break;

            case SDLK_RIGHT:
                _turnRight = true;
                break;

            default:
                break;
        }
    }
    else if( e.type == SDL_KEYUP )
    {
        switch( e.key.keysym.sym )
        {
            case SDLK_UP:
                _accelerate = false;
                break;

            case SDLK_DOWN:
                _brake = false;
                break;

            case SDLK_LEFT:
                _turnLeft = false;
                break;

            case SDLK_RIGHT:
                _turnRight = false;
                break;

            default:
                break;
        }
    }

}


void Player::move(double delta) {
    // Check level boundaries for player
    if (_x < 0) {_x = 0;}
    else if (_x > SCREEN_WIDTH) _x = SCREEN_WIDTH;

    if (_y < 0) {_y = 0;}
    else if (_y > SCREEN_HEIGHT) _y = SCREEN_HEIGHT;


    if (_accelerate) {
        if (_velocity < PLAYER_MAX_VELOCITY)
            _velocity += PLAYER_MAX_ACCELERATION * delta;
    }

    if (_brake) {
        if (_velocity > 0) {
            _velocity -= PLAYER_MAX_ACCELERATION * delta;
        }
        else if (SDL_GetTicks() - _reverseTimer > 600 && _velocity < 0 && _velocity > -0.5 * PLAYER_MAX_VELOCITY) {
            _velocity -= PLAYER_MAX_ACCELERATION * delta;
        }
    }

    _velocity -= _velocity * delta;

    if ((_velocity > PLAYER_MAX_VELOCITY/100 || _velocity < -PLAYER_MAX_VELOCITY/100)) {
        if (_turnLeft){
            _rotation -= PLAYER_MAX_ROTATION_SPEED * _velocity/PLAYER_MAX_VELOCITY * delta;
        }
        else if (_turnRight) {
            _rotation += PLAYER_MAX_ROTATION_SPEED * _velocity/PLAYER_MAX_VELOCITY * delta;
        }
    }

    Car::move(delta);
}

void Player::reset() {
    _accelerate = false;
    _brake = false;
    _turnLeft = false;
    _turnRight = false;

    _velocity = 0;
}
