#include "Menu.h"
#include <utility>

Menu::Menu() = default;


Menu::Menu(std::list<menuItem> options) {
    _options = options;
}

menuItem Menu::createMenuItem(std::string name, int action) {
    menuItem m;

    m.name = name;
    m.action = action;
    m.go = GameObject();
    m.go.createFromText(name, TTF_OpenFont( "resources/orena.ttf", 50 ));

    return m;
}

void Menu::render() {
    for (menuItem& mi : _options)
    {
        if (mi.enabled) {
            mi.active ? mi.go.getSprite()->setColor({255,0,0}) : mi.go.getSprite()->setColor({0,0,0});
            mi.go.Render();
        }
    }
}

void Menu::open(bool roundInProgress) {
    int enabledCounter = 0;

    // Set which menu items will be visible
    for (menuItem& mi : _options)
    {
        if (roundInProgress) {
            mi.enabled = !(mi.action == START_GAME);
        }
        else
        {
            mi.enabled = !(mi.action == RESUME_GAME || mi.action == STOP_GAME);
        }

        if (mi.enabled) enabledCounter++;

        // Clear activity state
        mi.active = false;
    }

    // Set the first enabled item to active
    for (menuItem& mi : _options)
    {
        if (mi.enabled) {
            mi.active = true;
            _state = mi.action;
            break;
        }
    }

    // Set positions of enabled menu items
    double delta = SCREEN_HEIGHT/enabledCounter;
    int i = 1;
    for (menuItem& mi : _options)
    {
        if (mi.enabled) {
            mi.go.setPosition(SCREEN_WIDTH/2, i*delta - 0.5*delta);
            i++;
        }
    }

    _visible = true;
}

void Menu::moveUp() {
    for (menuItem& mi : _options)
    {
        if (mi.enabled) {
            if (mi.active) return;
            else break;
        }
    }

    for (auto it = _options.begin(); it != _options.end(); it++)
    {
        if (it->active) {
            it->active = false;

            // Find next enabled
            while (!((--it)->enabled));

            it->active = true;
            _state = it->action;
            break;
        }
    }
}

void Menu::moveDown() {
    if (_options.back().active) return;

    for (auto it = _options.begin(); it != _options.end(); it++)
    {
        if (it->active) {
            it->active = false;

            // Find next enabled
            while (!((++it)->enabled));

            it->active = true;
            _state = it->action;
            break;
        }
    }
}

int Menu::processEvents(SDL_Event e) {
    if( e.type == SDL_KEYUP )
    {
        switch( e.key.keysym.sym )
        {
            case SDLK_UP:
                moveUp();
                break;

            case SDLK_DOWN:
                moveDown();
                break;

            case SDLK_RETURN:
                return confirm();

            case SDLK_ESCAPE:
                close();
                return RESUME_GAME;

            default:
                break;
        }
    }

    return NONE;
}
