#ifndef BALONIK_BALOON_H
#define BALONIK_BALOON_H


#include "Car.h"

/**
 * Defines class of Baloon - object to catch by the Player.
 */
class Baloon : public Car {
public:
    Baloon();
    /**
     * Create Baloon instance at position.
     * @param x
     * @param y
     */
    Baloon(int x, int y);
    /**
     * Overrides Car method - first modify frantic movement of the Baloon and then use Car move method.
     * @param delta Time difference between current and last frame.
     */
    void move(double delta);
    /**
     * Add velocity to the Baloon. Used when Baloon collides with Cars to simulate wind impact on Baloon.
     * @param inV
     */
    void addVelocity(int inV);
private:
    std::mt19937 _seed;
};


#endif //BALONIK_BALOON_H
