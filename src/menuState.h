#ifndef BALONIK_GAMESTATE_H
#define BALONIK_GAMESTATE_H

/**
 * Describes states chosen by using menu.
 */
enum menuState {
    NONE,
    START_GAME,
    RESUME_GAME,
    STOP_GAME,
    EXIT_GAME
};

#endif