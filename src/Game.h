#ifndef BALONIK_GAME_H
#define BALONIK_GAME_H

//#define DEBUG_MODE

#include <list>
#include <SDL.h>
#include <random>
#include <sstream>
#include "GameObject.h"
#include "Player.h"
#include "Car.h"
#include "Baloon.h"
#include "menuState.h"
#include "Menu.h"

extern const bool FRAME_LIMITER;
extern const float GRASS_WIDTH;
extern const float STREET_WIDTH;
extern const int MAX_ROUND;
extern const int CAR_MAX_VELOCITY;
extern const int CAR_MAX_COUNT;
extern const SDL_Point CAR_SPAWN_POINTS[];
extern const std::string CAR_TEXTURES[];


/**
 * Main class of the program.
 */
class Game {
public:
    /**
     * Game constructor needs screen width, screen height and a frame rate which it will try to hit
     * if VSYNC is not available (like in VM for example).
     * @param screenWidth
     * @param screenHeight
     * @param frameRate
     */
    Game(int screenWidth, int screenHeight, int frameRate);
    Game();
    /**
     * Main function of the main class. Initializes needed librarites and starts the game loop.
     */
    void Run();

private:
    bool Initialize();
    void Shutdown();
    void Frame();
    void ProcessEvents(SDL_Event e);
    void checkCollisions(double dT);
    void spawnCar();
    void spawnCar(SDL_Point p);
    void Render();
    void newGame();
    void stopGame();
    void showEndScore();
    std::mt19937 _seed;
    Menu _menu;
    SDL_Event _event;
    bool _running = true;
    int _screenWidth = 800;
    int _screenHeight = 600;
    int _frameRate = 60;
    Uint32 _lastFrameTime = 0;
    std::list<GameObject*> _gameObjectList = {};
    std::list<Car> _carList;
    Player _p;

    int _points = 0;
    int _round = 0;
    GameObject _goPoints;
    GameObject _goRound;
    GameObject _goFinalScore;
    std::stringstream _pointsText;
    std::stringstream _roundText;
    std::stringstream _finalScoreText;
    void endRound(bool success);
    void resetGame();

    Baloon _b;
    bool _playerCarCollision = false;

    bool _gameInProgress = false;
    bool _roundInProgress = false;
    bool _paused = false;
    bool _gameEnded = false;

    Uint32 _lastCarSpawningTime[8] = {0};
};


#endif //BALONIK_GAME_H
