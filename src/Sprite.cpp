#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include "Sprite.h"


Sprite::Sprite() = default;

Sprite::Sprite(const std::string path, int x, int y, double angleOrigin, SpriteType sType, TTF_Font* font) {
    _surfRect.x = x;
    _surfRect.y = y;
    _surfRect.w = 0;
    _surfRect.h = 0;

    _angleOrigin = angleOrigin;
    _path = path;
    _font = font;
    _sType = sType;

    modifyTexture(path, sType, font);
}


void Sprite::modifyTexture(std::string path, SpriteType sType, TTF_Font* font) {
    SDL_Surface* loadedSurface = nullptr;

    if (sType == SPRITE_TYPE_TEXT) {
        loadedSurface = TTF_RenderText_Solid( font ? font : _font, path.c_str(), _color );
    }
    else
    {
        loadedSurface = IMG_Load( path.c_str() );
    }

    if( loadedSurface == nullptr )
    {
        printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
    }
    else
    {
        createTexture(loadedSurface, path);

        //Get rid of old loaded surface
        SDL_FreeSurface( loadedSurface );
    }
}

void Sprite::createTexture(SDL_Surface* loadedSurface, const std::string path) {
    freeTexture();

    _texture = SDL_CreateTextureFromSurface( gRenderer , loadedSurface );
    if( _texture == nullptr )
    {
        printf( "Unable to optimize image %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
    }
    else
    {
        SDL_QueryTexture(_texture, nullptr, nullptr, &_surfRect.w, &_surfRect.h);
    }
}

void Sprite::freeTexture() {
    if( _texture != nullptr )
    {
        SDL_DestroyTexture( _texture );
        _texture = nullptr;
        _surfRect.h = 0;
        _surfRect.w = 0;
    }
}

void Sprite::Render() {
    SDL_RenderCopyEx( gRenderer, _texture, nullptr, &_surfRect, _angleOrigin + _angle, nullptr, SDL_FLIP_NONE );
}
