#include <SDL_ttf.h>
#include "Game.h"
#include <random>



//Screen dimension constants and framerate (if VSYNC is not available)
const int SCREEN_WIDTH = 800; // px
const int SCREEN_HEIGHT = 600; // px
const int FRAME_RATE = 60; // FPS
const bool FRAME_LIMITER = false; // try to limit frame count

const int MAX_ROUND = 10; // Each game will take this amount of rounds.

const int CAR_MAX_VELOCITY = 400;
const int CAR_MAX_COUNT = 16; // How many cars are on the screen.

const int PLAYER_MAX_VELOCITY = 250;
const int PLAYER_MAX_ACCELERATION = 200;
const int PLAYER_MAX_ROTATION_SPEED = 250;

// Describes where cars will have their lanes - if modified, background image will need modification too!
const float GRASS_WIDTH = 10.f / 100 * SCREEN_HEIGHT;
const float STREET_WIDTH = 70.f / 100 * SCREEN_HEIGHT / 8;

// Where to spawn new cars - starting points of lanes.
const SDL_Point CAR_SPAWN_POINTS[] = {
        {SCREEN_WIDTH + 50, int(GRASS_WIDTH + 0.5*STREET_WIDTH)},
        {SCREEN_WIDTH + 50, int(GRASS_WIDTH + 1.5*STREET_WIDTH)},
        {SCREEN_WIDTH + 50, int(GRASS_WIDTH + 2.5*STREET_WIDTH)},
        {SCREEN_WIDTH + 50, int(GRASS_WIDTH + 3.5*STREET_WIDTH)},
        {-50, int(SCREEN_HEIGHT - GRASS_WIDTH - 3.5*STREET_WIDTH)},
        {-50, int(SCREEN_HEIGHT - GRASS_WIDTH - 2.5*STREET_WIDTH)},
        {-50, int(SCREEN_HEIGHT - GRASS_WIDTH - 1.5*STREET_WIDTH)},
        {-50, int(SCREEN_HEIGHT - GRASS_WIDTH - 0.5*STREET_WIDTH)}
};

// Paths to the images of cars which will become car's textures.
const std::string CAR_TEXTURES[] = {
        "resources/car-truck1.png",
        "resources/car-truck2.png",
        "resources/car-truck3.png",
        "resources/car-truck4.png",
        "resources/car-truck5.png"
};



// SDL Library Globals
SDL_Window* gWindow = nullptr;
SDL_Renderer* gRenderer = nullptr;
TTF_Font* gFont = nullptr;


int main( int argc, char* args[] )
{
    Game game = Game(SCREEN_WIDTH, SCREEN_HEIGHT, FRAME_RATE);

    game.Run();

    return 0;
}