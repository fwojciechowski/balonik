#ifndef BALONIK_MENUITEM_H
#define BALONIK_MENUITEM_H

#include <string>
#include "menuState.h"
#include "GameObject.h"

/**
 * Struct to define menu items.
 */
struct menuItem {
    std::string name = ""; // name used for Sprite
    int action = NONE; // action that will be taken if chosen
    bool active = false; // is menu item currently selected?
    bool enabled = false; // should menu item be visible in the current menu state?
    GameObject go; // holds GameObject object with sprite, position etc.
};

#endif //BALONIK_MENUITEM_H
