#ifndef BALONIK_CAR_H
#define BALONIK_CAR_H

#include "GameObject.h"

//#define DEBUG_MODE

extern const int FRAME_RATE;

/**
 * Car class describes GameObjects which move.
 */
class Car : public GameObject {
public:
    Car();
    Car(int x, int y);
    /**
     * Function describes basic movement of the instance. Uses object's velocity and rotation. Calculations are made
     * using time difference between previous and current frame.
     * @param delta - time difference between last and current frame [ms].
     */
    virtual void move(double delta);
    /**
     * Renders Sprite of the GameObject. In Debug Mode also renders bounding box.
     */
    void Render();
    /**
     * Sets velocity of the object.
     * @param velocity
     */
    void setVelocity(double velocity);
    /**
     * Sets rotation of the object to set direction in which it will move. Also modifies Sprite
     * rotation to reflect this.
     * @param rotation
     */
    void setRotation(double rotation);
    /**
     * Initializes bounding box for collision checking. It is a rectangle with width and length
     * of uses Sprite.
     */
    void initBoundingBox();
    /**
     * Function returns bounding box of the object.
     * @return bounding box as SDL_Rect* structure.
     */
    SDL_Rect* getBoundingBox() { return &_bBox; }
    /**
     * Refreshes bounding box. Use when object is moving.
     */
    void refreshBoundingBox();

protected:
    /**
     * Rotation of the object.
     */
    double _rotation = 0;
    /**
     * Velocity of the object.
     */
    double _velocity = 0;
    /**
     * SDL_Rect struct descibing bounding box.
     */
    SDL_Rect _bBox;
};


#endif //BALONIK_CAR_H
