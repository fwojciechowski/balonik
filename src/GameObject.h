#ifndef BALONIK_GAMEOBJECT_H
#define BALONIK_GAMEOBJECT_H

#include <SDL_ttf.h>
#include "Sprite.h"

extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;


/**
 * Base Object in the game for any visible instances like cars, menu items and player. Represents physical objects.
 */
class GameObject {
public:
    /**
     * Initializes GameObject with basic parameters.
     */
    GameObject();
    /**
     * Initializes GameObject at position.
     * @param x width parameter of the position.
     * @param y height parameter of the position.
     */
    GameObject(int x, int y);
    /**
     * Create GameObject from image.
     * @param path Path to the image.
     * @param angle Angle of the image. Default - pointed downward.
     */
    void createFromImage(std::string path, double angle = 0);
    /**
     * Create GameObject from Text. Creates Sprite using given text and font.
     * @param path Text to use.
     * @param font Font to use.
     */
    void createFromText(std::string path, TTF_Font* font = gFont);
    /**
     * Renders GameObject's Sprite.
     */
    virtual void Render() { _s.Render(); }
    /**
     * Sets position of the GameObject. If object is out of bounds it will be marked for death -
     * - will be removed during next frame.
     * @param x
     * @param y
     */
    void setPosition(double x, double y);
    /**
     * Sets rotation of the object. Sets direction the instance will move to.
     * @param angle degrees
     */
    void setAngle(double angle) { _angle = angle; _s.setAngle(_angle); };
    /**
     * Modifies text of the Sprite Type Text.
     * @param text New text for the GameObject's Sprite.
     */
    void setText( std::string text );
    /**
     * Returns x position.
     * @return x position.
     */
    double getPositionX() {return _x; }
    /**
     * Returns y position.
     * @return y position.
     */
    double getPositionY() {return _y; }
    /**
     * Get Sprite of the GameObject.
     * @return Pointer to the Sprite of the GameObject.
     */
    Sprite* getSprite() {return &_s;}
    /**
     * Check if GameObject is alive - that is needs to be rendered, taken care of etc.
     * @return bool alive.
     */
    bool isAlive() { return _alive; }
    /**
     * If GameObject is out of bounds, mark it for death - will be removed during next frame.
     */
    void markForDeath() { _alive = false; }

protected:
    /**
     * X position - floating point for smooth movement.
     */
    double _x = 0;
    /**
     * Y position - floating point for smooth movement.
     */
    double _y = 0;
    /**
     * Angle for GameObject's Sprite rotation.
     */
    double _angle = 0;

private:
    Sprite _s;
    std::string _text;
    bool _alive = true;
};


#endif //BALONIK_GAMEOBJECT_H
