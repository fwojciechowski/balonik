#ifndef BALONIK_MENU_H
#define BALONIK_MENU_H

#include <list>
#include <unordered_map>
#include "menuItem.h"

/**
 * Describes Menu class to control game flow.
 */
class Menu {
public:
    Menu();
    /**
     * Create Menu using menuItems from given list.
     * @param options
     */
    Menu(std::list<menuItem> options);
    /**
     * Creates menuItems which can later be used to create Menu instances.
     * @param name Name of the option, used to later create Sprite with this text.
     * @param action What action will be taken if this item will be the chosen one?
     * @return menuItem struct.
     */
    static menuItem createMenuItem(std::string name, int action);
    /**
     * Render menu -> render each menuItem which is enabled. Select active item.
     */
    void render();
    /**
     * Process keyboard events if Menu is open.
     * @param e SDL event description.
     * @return Chosen menu state (if any), ie. Start Game, Stop Game.
     */
    int processEvents(SDL_Event e);
    /**
     * Try to move one option up.
     */
    void moveUp();
    /**
     * Try to move one option down.
     */
    void moveDown();
    /**
     * Closes window and sends chosen option to the caller.
     * @return Chosen option, ie. Start Game, Stop Game.
     */
    int confirm() { close(); return _state;};
    /**
     * Open Menu. Pause the game if round is in progress.
     * @param roundInProgress
     */
    void open(bool roundInProgress);
    /**
     * Is Menu instance open?
     * @return bool whether Menu is open or not.
     */
    bool isOpen() { return _visible; }
    /**
     * Close the menu, either by selecting option or Escaping.
     */
    void close() { _visible = false; };

private:
    int _state = NONE;
    std::list<menuItem> _options;
    bool _visible = false;
};


#endif //BALONIK_MENU_H
