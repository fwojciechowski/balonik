#ifndef BALONIK_SPRITE_H
#define BALONIK_SPRITE_H

#include <string>
#include <SDL.h>

extern SDL_Renderer* gRenderer;
extern TTF_Font* gFont;

/**
 * What type of is this Sprite?
 */
enum SpriteType {
    SPRITE_TYPE_IMAGE = 0,
    SPRITE_TYPE_TEXT = 1
};

/**
 * Class for holding image information for GameObjects. Basically a bridge between game and SDL library.
 */
class Sprite {
public:
    Sprite();
    /**
     * Create Sprite object using image or text and font.
     * @param path Path to the image or text to show.
     * @param x X position
     * @param y Y position
     * @param angleOrigin Modifier if images direction is different than 0 degrees (that is - Sprite 'looks down').
     * @param sType Type of the sprite, either image or text.
     * @param font Font to use if sprite is of type text.
     */
    Sprite(std::string path, int x, int y, double angleOrigin = 0, SpriteType sType = SPRITE_TYPE_IMAGE, TTF_Font* font = gFont);
    /**
     * Render sprite by copying its texture over part of SDL Renderer.
     */
    void Render();
    /**
     * Set position of the Sprite - origin will be at the centre of the image.
     * @param x
     * @param y
     */
    void setPosition(int x, int y) { _surfRect.x = x - _surfRect.w/2; _surfRect.y = y - _surfRect.h/2; }
    /**
     * Rotate Sprite by setting new angle.
     * @param angle
     */
    void setAngle(double angle) { _angle = angle; }
    /**
     * Set color of the Sprite type Text.
     * @param color
     */
    void setColor(SDL_Color color) { _color = color; refresh(); }
    /**
     * Set or update texture of the Sprite. Use when text has changed or new image is needed.
     * @param path Path to the image or new text to show.
     * @param sType Type of the sprite, either image or text.
     * @param font Font to use if Sprite is of type text.
     */
    void modifyTexture(std::string path, SpriteType sType = SPRITE_TYPE_IMAGE, TTF_Font* font = nullptr);
    /**
     * Get Sprite Width.
     * @return Sprite Width in px.
     */
    const int getSpriteWidth() { return _surfRect.w; }
    /**
     * Get Sprite Height.
     * @return Sprite Height in px.
     */
    const int getSpriteHeight() { return _surfRect.h; }

private:
    std::string _path = "";
    SpriteType _sType = SPRITE_TYPE_IMAGE;
    SDL_Rect _surfRect;
    double _angle = 0;
    double _angleOrigin = 0; // modifier for rotating texture
    SDL_Texture* _texture = nullptr;
    void createTexture(SDL_Surface* loadedSurface, std::string path);
    void freeTexture();
    void refresh() { modifyTexture(_path, _sType, _font); }
    TTF_Font* _font = gFont;
    SDL_Color _color = {0, 0, 0};
};


#endif //BALONIK_SPRITE_H
